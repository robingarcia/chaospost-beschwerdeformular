# Chaos-Post Beschwerdeformular

Das Chaos-Post Beschwerdeformular in LaTeX als aufüllbares PDF mit digitalem Signaturfeld.

Benötigt folgende Pakete:
* expl3
* xcoffins
* geometry
* fontspec
* color
* ragged2e
* hyperref
* eforms (nicht in TeXLive enthalten, aber auf CTAN erhältlich)

Als Schrift wird IBM Plex Sans verwendet.

# ToDo
* Maße korrigieren, alles noch ein bißchen schebbs...
* Siegel als Option
* Kopierer-Look